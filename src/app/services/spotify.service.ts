import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable( {
  providedIn: 'root'
} )
export class SpotifyService {

  miToken = 'Bearer BQBKWJUb2oF5bhqPq2FaZQdibr7T52IgO20_k9emijYgi86sOtPDVhfwYlJbc3vIq3X5e3FTDroJvkETAL0';

  constructor(private http: HttpClient) {
    console.log('Spotify service ready');
  }

  getQuery( query: string) {
    const url = `https://api.spotify.com/v1/${query}`;

    const headers = new HttpHeaders({
      Authorization: this.miToken
    });

    return this.http.get(url, {headers});

  }

  getNewReleases(){
    return this.getQuery('browse/new-releases')
                .pipe( map(  data  => data['albums'].items));
  }

  getArtistas(termino: string){
    return this.getQuery(`search?q=${termino}&type=artist&limit=15`)
                .pipe( map(  data  => data['artists'].items));
  }

  getArtista(id: string){
    return this.getQuery(`artists/${id}`);
  }

  getTopTracks(id: string){
    return this.getQuery(`artists/${id}/top-tracks?country=SE`)
      .pipe( map(  data  => data['tracks']));
  }

}
